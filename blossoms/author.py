from django.db import models


class Author(models.Model):
    """Base class to store author details."""

    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30)

    def __str__(self):
        return ' '.join([self.first_name, self.last_name])

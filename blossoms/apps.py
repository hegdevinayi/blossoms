# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class BlossomsConfig(AppConfig):
    name = 'blossoms'

    def ready(self):
        from .models import *

import datetime
from django.db import models
from django.core.validators import MinValueValidator
from django.core.validators import MaxValueValidator

from .author import Author


class Book(models.Model):
    """Base class to store info about a book."""

    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255, blank=True)
    year_published = models.PositiveSmallIntegerField('year published',
                                                      validators=[MaxValueValidator(datetime.date.today().year)],
                                                      blank=True, null=True)

    author = models.ForeignKey(Author, related_name='book_set', blank=True, null=True)

    def __str__(self):
        return self.title

# -*- coding: utf-8 -*-

import os

import django


# Set Django settings environment variable
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blossoms.db.settings")

# Initialize and load application
django.setup()

# Import all application models
from .models import *

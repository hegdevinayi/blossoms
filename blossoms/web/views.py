# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404

from blossoms.book import Book
from blossoms.author import Author


def home(request):
    template = 'home.html'
    list_of_books = Book.objects.all()
    context = {
        'list_of_books': list_of_books
    }
    return render(request, template, context)


def book(request, book_id):
    template = 'book.html'
    book = get_object_or_404(Book, id=book_id)
    context = {
        'book': book
    }
    return render(request, template, context)


def author(request, author_id):
    return HttpResponse('Author #{}'.format(author_id))
